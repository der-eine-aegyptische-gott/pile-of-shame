import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'
import AppLoading from 'expo-app-loading'
import { NavigationContainer } from '@react-navigation/native'
import { useColorScheme } from 'react-native'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import * as Sentry from 'sentry-expo'
import { DeviceEventEmitter, Text } from 'react-native'

import initDB from './src/database/initDB'
import deleteDB from './src/database/deleteDB'
import { RootStackScreen } from './src/views/navigation'
import { executeQuery } from './src/database/accessor'
import { dark, light, red } from './src/views/theme'
import { selectSetting } from './src/logic/SettingRepository'

Sentry.init({
  dsn:
    'https://edac0077415741d1b1f80982e8e2090d@o280504.ingest.sentry.io/5520291',
  enableInExpoDevelopment: true,
  debug: true,
  enableAutoSessionTracking: true,
  sessionTrackingIntervalMillis: 10000,
})

export default function App() {
  const colorScheme = useColorScheme()
  const [theme, setTheme] = useState(colorScheme)
  const [isReady, setisReady] = useState(false)
  const init = async () => {
    //await deleteDB()
    let check = await executeQuery(
      `SELECT name FROM sqlite_master WHERE type='table' AND name='Items' COLLATE NOCASE`
    )
    if (check.rows.length === 0) {
      await initDB()
    }
  }

  const loadFont = async () => {
    await Font.loadAsync({
      'arcade-classic': require('./assets/fonts/ArcadeClassic.ttf'),
      ...Ionicons.font,
    })
    setTimeout(function () {
      setisReady(true)
    }, 1000)
  }

  const getTheme = () => {
    const currentTheme = theme === 'default' ? colorScheme : theme
    switch (currentTheme) {
      case 'dark':
      default:
        return dark
      case 'light':
        return light
      case 'red':
        return red
    }
  }

  useEffect(() => {
    selectSetting().then((value) => {
      if (value.theme) {
        setTheme(value.theme)
      }
    })
    init()
    loadFont()
  }, [])

  DeviceEventEmitter.addListener('theme.event', () => {
    selectSetting().then((value) => {
      if (value.theme) {
        setTheme(value.theme)
      }
    })
  })

  if (!isReady) {
    return <AppLoading />
  }
  return (
    <SafeAreaProvider>
      <NavigationContainer theme={getTheme()}>
        <RootStackScreen />
        <StatusBar style="auto" />
      </NavigationContainer>
    </SafeAreaProvider>
  )
}
