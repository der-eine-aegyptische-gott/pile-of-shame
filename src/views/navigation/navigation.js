import React from 'react'
import { useTheme } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons, Foundation, Feather, AntDesign } from '@expo/vector-icons'

import { AddScreenModal } from '../components'
import { AddActionScreen } from '../screens/AddActionScreen'

import {
  HomeStackScreen,
  ListStackScreen,
  TagsStackScreen,
  SettingsStackScreen,
} from './stacks'

const BottomTab = createBottomTabNavigator()

export const Navigation = (props) => {
  const { navigation } = useTheme()
  return (
    <BottomTab.Navigator
      screenOptions={{
        headerShown: false,
      }}
      tabBarOptions={navigation}
    >
      <BottomTab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons
              name="ios-home"
              size={30}
              style={{ marginBottom: -3 }}
              color={color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="List"
        component={ListStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Foundation
              name="list"
              size={30}
              style={{ marginBottom: -3 }}
              color={color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Add"
        component={AddActionScreen}
        options={{
          tabBarButton: () => <AddScreenModal />,
        }}
      />
      <BottomTab.Screen
        name="Tags"
        component={TagsStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <AntDesign
              name="tags"
              size={30}
              style={{ marginBottom: -3 }}
              color={color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Settings"
        component={SettingsStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Feather
              name="settings"
              size={30}
              style={{ marginBottom: -3 }}
              color={color}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  )
}
