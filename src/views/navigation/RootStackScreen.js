import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Navigation } from './navigation'
import {
  ItemAddStackScreen,
  TagAddStackScreen,
  ItemDetailStackScreen,
  ItemEditStackScreen,
  TagEditStackScreen,
} from './stacks'
import {MultipleItemAdd} from "../screens/ItemScreens/MultipleItemAdd";

const Stack = createStackNavigator()

export const RootStackScreen = () => {
  return (
    <Stack.Navigator mode="modal">
      <Stack.Screen
        name="Main"
        component={Navigation}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ItemAdd"
        component={ItemAddStackScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TagAdd"
        component={TagAddStackScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ItemDetail"
        component={ItemDetailStackScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ItemEdit"
        component={ItemEditStackScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TagEdit"
        component={TagEditStackScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
          name="MultipleItemAdd"
          component={MultipleItemAdd}
          options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}
