import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { TagAddScreen } from '../../../screens/TagScreens'

const Stack = createStackNavigator()

export const TagAddStackScreen = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="TagAdd" component={TagAddScreen} />
    </Stack.Navigator>
  )
}
