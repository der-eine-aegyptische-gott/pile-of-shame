import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { TagEditScreen } from '../../../screens/TagScreens'

const Stack = createStackNavigator()

export const TagEditStackScreen = ({ route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="TagEdit"
        component={TagEditScreen}
        initialParams={{ tag: route.params.tag, from: route.params.from }}
      />
    </Stack.Navigator>
  )
}
