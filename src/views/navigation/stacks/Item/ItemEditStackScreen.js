import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { ItemEditScreen } from '../../../screens/ItemScreens'

const Stack = createStackNavigator()

export const ItemEditStackScreen = ({ route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="ItemEdit"
        component={ItemEditScreen}
        initialParams={{
          item: route.params.item,
          from: route.params.from,
        }}
      />
    </Stack.Navigator>
  )
}
