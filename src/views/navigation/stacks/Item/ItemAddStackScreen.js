import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { ItemAddScreen } from '../../../screens/ItemScreens'

const Stack = createStackNavigator()

export const ItemAddStackScreen = ({ route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="ItemAdd"
        component={ItemAddScreen}
        initialParams={{ category: route.params.category }}
      />
    </Stack.Navigator>
  )
}
