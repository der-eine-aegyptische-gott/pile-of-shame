import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { ItemDetailScreen } from '../../../screens/ItemScreens'

const Stack = createStackNavigator()

export const ItemDetailStackScreen = ({ route }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen
        name="ItemDetail"
        component={ItemDetailScreen}
        initialParams={{
          item: route.params.item,
          from: route.params.from,
        }}
      />
    </Stack.Navigator>
  )
}
