import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { ListScreen } from '../../screens/ListScreen'

const Stack = createStackNavigator()

export const ListStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="List" component={ListScreen} />
    </Stack.Navigator>
  )
}
