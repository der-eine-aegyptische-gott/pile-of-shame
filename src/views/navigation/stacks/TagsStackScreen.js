import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { TagsScreen } from '../../screens/TagsScreen'

const Stack = createStackNavigator()

export const TagsStackScreen = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Tags" component={TagsScreen} />
    </Stack.Navigator>
  )
}
