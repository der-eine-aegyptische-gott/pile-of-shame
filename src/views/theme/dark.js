export const dark = {
  colors: {},
  container: {
    backgroundColor: '#34495e',
  },
  itemCard: {
    backgroundColor: '#495C6E',
  },
  title: {
    fontFamily: 'arcade-classic',
    fontSize: 30,
    color: 'white',
  },
  text: {
    color: 'white',
  },
  button: {
    backgroundColor: '#495C6E',
  },
  buttonText: {
    color: 'white',
  },
  icon: {
    color: 'white',
  },
  marked: {
    color: 'yellow',
  },
  modal: {
    backgroundColor: '#34495e',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  textInput: {
    color: 'white',
  },
  textInputPlaceholderColor: '#ecf0f1',
  rating: {
    color: '#fbc531',
  },
  navigation: {
    activeTintColor: '#f1c40f',
    inactiveTintColor: 'white',
    labelStyle: {
      color: 'white',
    },
    style: {
      backgroundColor: 'rgb(44, 62, 80)',
    },
  },
  switch: {
    trackColor: {
      false: '#767577',
      true: '#f1c40f',
    },
    iosBackgroundColor: '#3e3e3e',
  },
}
