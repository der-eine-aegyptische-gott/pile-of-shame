export const light = {
  colors: {},
  container: {
    backgroundColor: '#ecf0f1',
  },
  itemCard: {
    backgroundColor: '#bdc3c7',
  },
  title: {
    fontFamily: 'arcade-classic',
    fontSize: 30,
    color: 'black',
  },
  text: {
    color: 'black',
  },
  button: {
    backgroundColor: '#bdc3c7',
  },
  buttonText: {
    color: 'black',
  },
  icon: {
    color: 'black',
  },
  marked: {
    color: 'red',
  },
  modal: {
    backgroundColor: '#ecf0f1',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  textInput: {
    color: 'black',
  },
  textInputPlaceholderColor: 'black',
  rating: {
    color: '#fbc531',
  },
  navigation: {
    activeTintColor: 'black',
    inactiveTintColor: '#bdc3c7',
    labelStyle: {
      color: 'black',
    },
    style: {
      backgroundColor: '#ecf0f1',
    },
  },
  switch: {
    trackColor: {
      false: '#767577',
      true: '#f1c40f',
    },
    iosBackgroundColor: '#3e3e3e',
  },
}
