export const red = {
  colors: {},
  container: {
    backgroundColor: '#e74c3c',
  },
  itemCard: {
    backgroundColor: '#c0392b',
  },
  title: {
    fontFamily: 'arcade-classic',
    fontSize: 30,
    color: '#ecf0f1',
  },
  text: {
    color: '#ecf0f1',
  },
  button: {
    backgroundColor: '#a33c31',
  },
  buttonText: {
    color: '#ecf0f1',
  },
  icon: {
    color: '#ecf0f1',
  },
  marked: {
    color: 'yellow',
  },
  modal: {
    backgroundColor: '#c0392b',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  textInput: {
    color: '#ecf0f1',
  },
  textInputPlaceholderColor: '#ecf0f1',
  rating: {
    color: '#fbc531',
  },
  navigation: {
    activeTintColor: '#7f8c8d',
    inactiveTintColor: '#ecf0f1',
    labelStyle: {
      color: '#ecf0f1',
    },
    style: {
      backgroundColor: '#e74c3c',
    },
  },
  switch: {
    trackColor: {
      false: '#767577',
      true: '#f1c40f',
    },
    iosBackgroundColor: '#3e3e3e',
  },
}
