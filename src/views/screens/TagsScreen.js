import React, { useEffect, useState } from 'react'

import { Accordion, SearchBar, Screen, ChangeTagsModal } from '../components'

import { groupItemsByTags } from '../../logic/util/helper/itemHelper'
import { deleteTagById } from '../../logic/TagRepository'
import { DeleteModal } from '../components/Modals/DeleteModal'

export const TagsScreen = ({ navigation }) => {
  const [tags, setTags] = useState([])
  const [chosenTag, setChosenTag] = useState({ name: 'Default' })
  const [searchQuery, setSearchQuery] = useState('')
  const [modalVisible, setModalVisible] = useState(false)

  const deleteModal = (tag) => {
    setChosenTag(tag)
    setModalVisible(true)
  }
  const closeModal = () => {
    setModalVisible(false)
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setSearchQuery('')
      let timer = setTimeout(() => {
        groupItemsByTags().then((tags) => {
          setTags(tags)
        })
      }, 500)
      return () => {
        clearTimeout(timer)
      }
    })
    return unsubscribe
  }, [])

  const navToDetail = (item) => {
    navigation.navigate('ItemDetail', {
      item: item,
      from: 'Tags',
    })
  }

  const navtoEdit = (tag) => {
    navigation.navigate('TagEdit', {
      tag: tag,
      from: 'Tags',
    })
  }

  const deleteTag = (id) => {
    deleteTagById(id).then((r) =>
      groupItemsByTags().then((tags) => {
        setTags(tags)
      })
    )
  }

  const filterTag = function (tags) {
    return tags.filter((value) =>
      value.name.toLowerCase().includes(searchQuery.toLowerCase())
    )
  }

  return (
    <Screen
      headerChild={
        <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} text="Search tag..." />
      }
      bodyChild={
        <>
          <DeleteModal
            title={`Delete ${chosenTag.name}?`}
            closeModalHandler={closeModal}
            visible={modalVisible}
            currentItem={chosenTag}
            deleteHandler={deleteTag}
          />
          {filterTag(tags).map((tag, index) => {
            return (
              <Accordion
                key={index}
                name={tag.name}
                icon={tag.icon}
                items={tag.items}
                onItemPress={navToDetail}
                isTag={true}
                editTagHandler={() => navtoEdit(tag)}
                deleteTagHandler={() => deleteModal(tag)}
              />
            )
          })}
        </>
      }
    />
  )
}
