import React, {useEffect, useState} from 'react'
import {selectCategoryById} from '../../../logic/CategoryRepository'

import {DetailHeader, ItemAdd, Screen} from '../../components'
import {Item} from "../../../logic/model/Item";

export const ItemAddScreen = ({ route, navigation }) => {
  const categoryId = route.params.category
  const [formCategory, setFormCategory] = useState({
    id: 0,
    icon: '',
    title: '',
  })
  const [multipleItems, setMultipleItems] = useState([])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      selectCategoryById(categoryId).then((categoryItem) =>
        setFormCategory(categoryItem)
      )
      if(!route.params.multipleItems === undefined){
        setMultipleItems(route.params.multipleItems)
      }
    })
    return unsubscribe
  }, [navigation])

  const handleSubmit = async ({
    category,
    title,
    image,
    description,
    releaseDate,
  }) => {
    if (title !== '') {
      let timestamp = new Date(releaseDate).getTime() / 1000
      let newItem = new Item(null,title,description,category,null,'',image,timestamp,null)
      multipleItems.push(newItem)

      navigation.navigate('MultipleItemAdd', {
        newItem,
        multipleItems
      })
    }
  }

  return (
    <Screen
      headerChild={
        <DetailHeader
          onIconPressHandler={() => navigation.goBack()}
          title={`Back`}
        />
      }
      bodyChild={
        <ItemAdd category={formCategory} handleSubmit={handleSubmit} />
      }
    />
  )
}
