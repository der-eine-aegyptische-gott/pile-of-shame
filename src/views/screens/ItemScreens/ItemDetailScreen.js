import React, { useState, useEffect } from 'react'
import { Image } from 'react-native'

import Placeholder from '../../../../assets/pile_of_shame_placeholder.png'

import { deleteItemById } from '../../../logic/ItemRepository'

import {
  ItemDetail,
  DetailHeader,
  ItemDetailButtons,
  Screen,
} from '../../components'
import { DeleteModal } from '../../components/Modals/DeleteModal'

export const ItemDetailScreen = ({ route, navigation }) => {
  const [detailItem, setDetailItem] = useState({})
  const item = route.params.item || {}
  const from = route.params.from || ''
  const [modalVisible, setModalVisible] = useState(false)
  const itemImgPath = item.imgPath || Image.resolveAssetSource(Placeholder).uri

  const navToEdit = () => {
    navigation.navigate('ItemEdit', {
      item: item,
      from: 'ItemDetail',
    })
  }
  const editTags = (tag) => {
    navigation.navigate('TagEdit', {
      tag: tag,
      from: from,
    })
  }

  const deleteModal = () => {
    setModalVisible(true)
  }
  const closeModal = () => {
    setModalVisible(false)
  }

  const deleteItem = (id) => {
    deleteItemById(id).then(() => navigation.navigate('Home'))
  }
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setDetailItem(item)
    })
    return unsubscribe
  }, [navigation])

  return (
    <Screen
      headerChild={
        <DetailHeader
          onIconPressHandler={() => navigation.goBack()}
          buttons={
            <ItemDetailButtons
              editHandler={() => navToEdit()}
              deleteHandler={() => deleteModal(item)}
            />
          }
          title={`Back to ${from}`}
        />
      }
      bodyChild={
        <>
          <DeleteModal
            title={`Delete ${item.title}?`}
            closeModalHandler={closeModal}
            visible={modalVisible}
            currentItem={item}
            fromView={'Tags'}
            deleteHandler={deleteItem}
          />
          <ItemDetail
            item={item}
            fromView={from}
            itemImgPath={itemImgPath}
            tagEditHandler={editTags}
          />
        </>
      }
    />
  )
}
