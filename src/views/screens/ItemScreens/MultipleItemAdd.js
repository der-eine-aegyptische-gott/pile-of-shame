import React, { useEffect, useState } from 'react'
import { selectCategoryById } from '../../../logic/CategoryRepository'
import { addItem } from '../../../logic/ItemRepository'

import {ItemAdd, Screen, DetailHeader, Button} from '../../components'
import {Text, View} from "react-native";

export const MultipleItemAdd = ({ route, navigation }) => {

    const [multipleItems, setMultipleItems] = useState(route.params.multipleItems)
    const item = route.params

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {

        })
        return unsubscribe
    }, [navigation])


    const handleAddMore = () => {
        navigation.navigate('ItemAdd',{
            multipleItems
        })
    }

    const handleDone = () => {
        console.log(multipleItems)
        multipleItems.forEach(async function(element){
            await addItem(
                element.title,
                element.description,
                element.category,
                '',
                '',
                element.imgPath,
                element.releaseDate,
                '',
                ''
            ).then(() => {
                navigation.navigate('Home')
            })
        }
        )

    }

    return (

        <View>
            <Button text={'another one'} handlePress={handleAddMore} />
            <Button text={'done'} handlePress={handleDone} />
        </View>

    )

}
