import React from 'react'

import { updateItemObject } from '../../../logic/ItemRepository'

import { ItemEdit, Screen, DetailHeader } from '../../components'

export const ItemEditScreen = ({ route, navigation }) => {
  const item = route.params.item || {}
  const fromView = route.params.from || ''

  const handleSubmit = async (item) => {
    if (item.title !== '') {
      const edit = await updateItemObject(item)
      if (edit) {
        navToOrigin()
      }
    }
  }

  const navToOrigin = () => {
    navigation.navigate('Home')
  }

  return (
    <Screen
      headerChild={
        <DetailHeader
          onIconPressHandler={() => navigation.goBack()}
          title={`Back`}
        />
      }
      bodyChild={<ItemEdit item={item} handleSubmit={handleSubmit} />}
    />
  )
}
