import React, { useEffect, useState } from 'react'
import { StyleSheet } from 'react-native'
import { useTheme } from '@react-navigation/native'
import { DeviceEventEmitter } from 'react-native'
import { Screen, HeaderTitle, Settings, Button } from '../components'

import { selectTags } from '../../logic/TagRepository'
import { selectSetting } from '../../logic/SettingRepository'
import { updateSettingObject } from '../../logic/SettingRepository'
import { exportDataBase, importDataBase } from '../../logic/DatabaseRepository'

export const SettingsScreen = ({ navigation }) => {
  const [settings, setSettings] = useState()
  const [theme, setTheme] = useState()
  const [homeTag, setHomeTag] = useState()
  const [homeMostRecent, setHomeMostRecent] = useState()
  const [tags, setTags] = useState([])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      selectSetting().then((value) => {
        setSettings(value)
        setTheme(value.theme)
        setHomeTag(value.tag)
        setHomeMostRecent(!!value.mostrecent)
      })
      selectTags().then((value) => {
        setTags(value)
      })
    })
    return unsubscribe
  })

  const saveTheme = async (value) => {
    setTheme(value)
    await updateSettingObject({
      ...settings,
      theme: value,
      mostrecent: homeMostRecent,
      tag: homeTag,
    })
    DeviceEventEmitter.emit('theme.event')
  }
  const saveHomeMostRecent = async (value) => {
    setHomeMostRecent(value ? 1 : 0)
    await updateSettingObject({
      ...settings,
      theme: theme,
      mostrecent: value ? 1 : 0,
      tag: homeTag,
    })
  }

  const saveHomeTag = async (value) => {
    setHomeTag(value)
    await updateSettingObject({
      ...settings,
      theme: theme,
      mostrecent: homeMostRecent,
      tag: value,
    })
  }

  return (
    <Screen
      headerChild={<HeaderTitle text="Settings" />}
      bodyChild={
        <>
          <Settings
            theme={theme}
            saveTheme={saveTheme}
            homeMostRecent={!!homeMostRecent}
            saveHomeMostRecent={saveHomeMostRecent}
            homeTag={homeTag}
            saveHomeTag={saveHomeTag}
            tags={tags}
            importHandler={importDataBase}
            exportHandler={exportDataBase}
          />
        </>
      }
    />
  )
}
