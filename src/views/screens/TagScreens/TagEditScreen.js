import React from 'react'
import { updateTagObject } from '../../../logic/TagRepository'

import { TagEdit, ScreenWithOutScroll, DetailHeader } from '../../components'

export const TagEditScreen = ({ route, navigation }) => {
  const tag = route.params.tag || {}
  const fromView = route.params.from || ''

  const navToOrigin = () => {
    if (fromView) {
      navigation.navigate(fromView)
    } else {
      navigation.navigate('Tags')
    }
  }

  const handleSubmit = async (tag) => {
    if(tag.name.trim()){
      await updateTagObject(tag).then(navToOrigin())
    }
  }

  return (
    <ScreenWithOutScroll
      headerChild={
        <DetailHeader onIconPressHandler={() => navToOrigin()} title={`Back`} />
      }
      bodyChild={<TagEdit tag={tag} handleSubmit={handleSubmit} />}
    />
  )
}
