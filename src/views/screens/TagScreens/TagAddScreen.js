import React from 'react'
import { addTag } from '../../../logic/TagRepository'
import { TagAdd, ScreenWithOutScroll, DetailHeader } from '../../components'

export const TagAddScreen = ({ navigation }) => {
  const handleSubmit = async ({ name, tooltip, color }) => {
    if(name.trim()) {
        await addTag(name, tooltip, color).then(navigation.navigate('Tags'))
    }
  }

  return (
    <ScreenWithOutScroll
      headerChild={
        <DetailHeader
          onIconPressHandler={() => navigation.goBack()}
          title={`Back`}
        />
      }
      bodyChild={<TagAdd handleSubmit={handleSubmit} />}
    />
  )
}
