import React, { useEffect, useState } from 'react'

import { Accordion, SearchBar, Screen } from '../components'

import { itemsGroupedByCategory } from '../../logic/util/helper/itemHelper'

export const ListScreen = ({ navigation }) => {
  const [catgoryItems, setCatgoryItems] = useState([])
  const [searchQuery, setSearchQuery] = useState('')

  const navToDetail = (item) => {
    navigation.navigate('ItemDetail', {
      item: item,
      from: 'List',
    })
  }

  useEffect(() => {
    /*itemsGroupedByCategory().then((data) => {
      setCatgoryItems(data)
    })*/
    const unsubscribe = navigation.addListener('focus', () => {
      setSearchQuery('')
      let timer = setTimeout(() => {
        itemsGroupedByCategory().then((data) => {
          setCatgoryItems(data)
        })
      }, 500 )
      return () => {
        clearTimeout(timer);
      }
    })
    return unsubscribe
  }, [])

  const filterItem = (items) =>
    items.filter((value) =>
      value.title.toLowerCase().includes(searchQuery.toLowerCase())
    )

  return (
    <Screen
      headerChild={
        <SearchBar
            searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          text="Search item in list..."
        />
      }
      bodyChild={
        <>
          {catgoryItems.map((item, index) => {
            return (
              <Accordion
                key={index}
                name={item.name}
                icon={item.icon}
                items={filterItem(item.items)}
                onItemPress={navToDetail}
                searchQuery={searchQuery}
              />
            )
          })}
        </>
      }
    />
  )
}
