import React, { useEffect, useState } from 'react'

import { Accordion, SearchBar, Screen } from '../components'

import { getItemsWithTag, getMostRecentItems } from '../../logic/ItemRepository'
import { selectSetting } from '../../logic/SettingRepository'

export const HomeScreen = ({ navigation }) => {
  const [mostRecentItems, setMostRecentItems] = useState([])
  const [tagItems, setTagItems] = useState([])
  const [mostrecent, setMostrecent] = useState(true)
  const [tag, setTag] = useState(null)
  const [searchQuery, setSearchQuery] = useState('')
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setSearchQuery('')
      let timer = setTimeout(() => {
        selectSetting().then((value) => {
          if (value.mostrecent) {
            setMostrecent(true)
            getMostRecentItems().then((value) => setMostRecentItems(value))
          } else {
            setMostrecent(false)
          }
          if (value.tag.id > 0) {
            setTag(value.tag)
            getItemsWithTag(value.tag).then((value) => setTagItems(value))
          } else {
            setTag(null)
            setTagItems([])
          }
        })
      }, 500)
      return () => {
        clearTimeout(timer)
      }
    })

    return unsubscribe
  }, [])

  const navToDetail = (item) => {
    navigation.navigate('ItemDetail', {
      item: item,
      from: 'Home',
    })
  }

  const buildTextQuery = function () {
    let text = 'Search in '
    if (tag && mostrecent) {
      text += tag.name + ' and most recent'
    } else if (mostrecent) {
      text += 'most recent'
    } else if (tag) {
      text += tag.name
    } else {
      text = ''
    }
    text += '..'
    return text
  }

  const filterItem = function (items) {
    return items.filter((value) =>
      value.title.toLowerCase().includes(searchQuery.toLowerCase())
    )
  }

  return (
    <Screen
      headerChild={
        <SearchBar searchQuery={searchQuery} setSearchQuery={setSearchQuery} text={buildTextQuery()} />
      }
      bodyChild={
        <>
          {mostrecent && (
            <Accordion
              name="Most recent"
              items={filterItem(mostRecentItems)}
              onItemPress={navToDetail}
              searchQuery={searchQuery}
            />
          )}
          {tag && (
            <Accordion
              name={tag.name}
              items={filterItem(tagItems)}
              onItemPress={navToDetail}
              searchQuery={searchQuery}
            />
          )}
        </>
      }
    />
  )
}
