import React from 'react'
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const Button = ({ text, handlePress, ...props }) => {
  const { button, buttonText } = useTheme()
  return (
    <View style={{ ...styles.container, ...props.styles }}>
      <TouchableWithoutFeedback onPress={handlePress}>
        <View style={{ ...styles.buttonContainer, ...button }}>
          <Text style={{ ...styles.buttonText, ...buttonText }}>{text}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { alignItems: 'flex-end' },
  buttonContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 5,
  },
  buttonText: { textAlign: 'center', fontWeight: 'bold', fontSize: 25 },
})
