import React from 'react'
import { StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { Entypo, AntDesign } from '@expo/vector-icons'
import { useTheme } from '@react-navigation/native'

export const AccordionTagIcons = ({ editTagHandler, deleteTagHandler }) => {
  const { icon } = useTheme()
  return (
    <>
      <TouchableWithoutFeedback onPress={editTagHandler}>
        <Entypo
          name="edit"
          size={25}
          color={icon.color}
          style={styles.button1}
        />
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={deleteTagHandler}>
        <AntDesign
          name="delete"
          size={25}
          color={icon.color}
          style={styles.button2}
        />
      </TouchableWithoutFeedback>
    </>
  )
}

const styles = StyleSheet.create({
  button1: {
    marginRight: 5,
  },
  button2: {
    marginRight: 15,
  },
})
