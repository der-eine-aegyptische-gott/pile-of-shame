import React from 'react'
import { ItemCard } from '../../ItemCard'

export const AccordionList = ({ items, onItemPress, searchQuery }) => {
  if (!items || items.length <= 0) {
    return <></>
  }
  return items.map((item, index) => (
    <ItemCard
      key={index}
      item={item}
      onPressHandler={onItemPress}
      searchQuery={searchQuery}
    />
  ))
}
