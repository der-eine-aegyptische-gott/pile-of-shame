import React from 'react'
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native'
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons'
import { useTheme } from '@react-navigation/native'
import { AccordionTagIcons } from './AccordionTagIcons'

export const AccordionTitle = ({
  name,
  icon,
  expand,
  setExpandHandler,
  isTag,
  editTagHandler,
  deleteTagHandler,
}) => {
  const theme = useTheme()
  return (
    <TouchableOpacity style={styles.box} onPress={() => setExpandHandler()}>
      <View style={styles.child}>
        {icon && (
          <MaterialCommunityIcons
            name={icon}
            size={25}
            style={styles.icon}
            color={theme.icon.color}
          />
        )}
        <Text style={theme.title}>{name}</Text>
      </View>
      <View style={styles.child}>
        {isTag && (
          <AccordionTagIcons
            editTagHandler={editTagHandler}
            deleteTagHandler={deleteTagHandler}
          />
        )}
        <AntDesign
          name={expand ? 'caretup' : 'caretdown'}
          size={25}
          color={theme.icon.color}
        />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'nowrap',
    marginBottom: 10,
  },
  child: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    marginRight: 10,
  },
})
