import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'

import { AccordionTitle } from './AccordionComponents/AccordionTitle'
import { AccordionList } from './AccordionComponents/AccordionList'

export const Accordion = ({
  name,
  icon,
  items,
  onItemPress,
  searchQuery,
  isTag,
  editTagHandler,
  deleteTagHandler,
}) => {
  const [expand, setExpand] = useState(true)
  return (
    <View style={styles.content}>
      <AccordionTitle
        name={name}
        icon={icon}
        expand={expand}
        isTag={isTag}
        setExpandHandler={() => setExpand(!expand)}
        editTagHandler={editTagHandler}
        deleteTagHandler={deleteTagHandler}
      />
      {expand && (
        <AccordionList
          items={items}
          onItemPress={onItemPress}
          searchQuery={searchQuery}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  content: { margin: 10 },
})
