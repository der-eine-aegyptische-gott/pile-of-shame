import React from 'react'
import { StyleSheet, View } from 'react-native'
import Constants from 'expo-constants'

export const Header = ({ children }) => {
  const getPaddingTop = () => Constants.statusBarHeight
  return (
    <View style={{ paddingTop: getPaddingTop(), ...styles.content }}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
  },
})
