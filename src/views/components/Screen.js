import React from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const Screen = ({ headerChild, bodyChild }) => {
  const { container } = useTheme()
  return (
    <View style={{ ...styles.container, ...container }}>
      {headerChild}
      <ScrollView style={styles.content}>{bodyChild}</ScrollView>
    </View>
  )
}

export const ScreenWithOutScroll = ({ headerChild, bodyChild }) => {
  const { container } = useTheme()
  return (
    <View style={{ ...styles.container, ...container }}>
      {headerChild}
      <View style={styles.content}>{bodyChild}</View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1 },
})
