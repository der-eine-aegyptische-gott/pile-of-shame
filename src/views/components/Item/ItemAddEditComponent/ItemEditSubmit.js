import React from 'react'

import { Button } from '../../Button'

export const ItemEditSubmit = ({ categoryId, handleSubmit }) => {
  const getTitle = (id) => {
    switch (id) {
      case 1:
        return 'Edit Game'
      case 2:
        return 'Edit Book'
      case 3:
        return 'Edit Movie'
      case 5:
        return 'Edit Music'
      default:
        return 'Edit Custom'
    }
  }
  return <Button text={getTitle(categoryId)} handlePress={handleSubmit} />
}
