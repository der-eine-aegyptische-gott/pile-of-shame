import React, { useEffect } from 'react'
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
} from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import { Entypo } from '@expo/vector-icons'
import { useTheme } from '@react-navigation/native'

export const ItemImage = ({ title, value, onChangeHandler }) => {
  const theme = useTheme()
  useEffect(() => {
    ;(async () => {
      if (Platform.OS !== 'web') {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync()
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!')
        }
      }
    })()
  }, [])

  const width = Dimensions.get('window').width / 2

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    })

    if (!result.cancelled) {
      onChangeHandler(result.uri)
    }
  }

  return (
    <>
      <View style={styles.container}>
        <Text style={{ ...styles.title, ...theme.text }}>{title}:</Text>
        <TouchableOpacity onPress={pickImage}>
          <ImageBackground
            source={{ uri: value }}
            style={{
              ...styles.image,
              width: width,
              height: width,
              borderColor: theme.text.color,
            }}
          >
            <View style={styles.imageContainer}>
              <Entypo name="edit" size={24} color="white" />
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    flex: 1,
    fontSize: 20,
  },
  imageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
})
