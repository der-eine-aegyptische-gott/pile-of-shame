import React from 'react'
import { StyleSheet, View, Button, Image, Text } from 'react-native'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

import Placeholder from '../../../../../../assets/pile_of_shame_placeholder.png'

export const ItemAddSearchItem = ({ item, useItemHandler }) => {
  const placeholderImage = Image.resolveAssetSource(Placeholder).uri

  return (
    <TouchableWithoutFeedback onPress={useItemHandler}>
      <View style={styles.container}>
        <Image
          source={{ uri: item.imgPath || placeholderImage }}
          style={styles.image}
        />
        <Text style={styles.text}>{item.title}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  text: {
    marginLeft: 5,
    fontSize: 15,
    color: 'black',
  },
  image: {
    width: 75,
    height: 75,
  },
})
