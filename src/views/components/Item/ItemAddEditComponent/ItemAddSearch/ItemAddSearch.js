import React, { useState } from 'react'
import { StyleSheet, View, TextInput, Text, Platform } from 'react-native'

import { searchGame } from '../../../../../logic/util/api/gamesService'
import { ItemAddSearchItem } from './ItemAddSearchItem'
import {searchMusic} from "../../../../../logic/util/api/musicService";

const HandleSearchData = ({
  searchData,
  itemSearchHandler,
  closeSearchedHandler,
}) => {
  if (!searchData) {
    return <></>
  }
  if (searchData.length <= 0) {
    return (
      <View style={styles.searchContainer}>
        <Text>no results</Text>
      </View>
    )
  }
  return (
    <View style={styles.searchContainer}>
      {searchData.map((item, index) => (
        <ItemAddSearchItem
          key={index}
          item={item}
          useItemHandler={() => {
            itemSearchHandler(item)
            closeSearchedHandler()
          }}
        />
      ))}
    </View>
  )
}

export const ItemAddSearch = ({ itemSearchHandler, searchCategory }) => {
  const [searchQuery, setSearchQuery] = useState('')
  const [searchData, setSearchData] = useState([])
  const [isSearched, setIsSearched] = useState(false)

  const search = (name) => {
    if (name && name.length > 2) {
      if(searchCategory.id === 1) {
        searchGame(name).then((data) => {
          setSearchData(data)
          setIsSearched(true)
        })
      }
      if(searchCategory.id === 5) {
        searchMusic(name).then((data) => {
          setSearchData(data)
          setIsSearched(true)
        })
      }
    }
  }

  return (
    <View style={styles.zindex}>
      <View style={styles.container}>
        <TextInput
          placeholder="Search on IGDB... "
          value={searchQuery}
          onChangeText={(text) => setSearchQuery(text)}
          onSubmitEditing={() => search(searchQuery)}
          style={styles.input}
        />
      </View>
      <View>
        {isSearched && (
          <HandleSearchData
            searchData={searchData}
            itemSearchHandler={itemSearchHandler}
            closeSearchedHandler={() => setIsSearched(false)}
          />
        )}
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
  searchContainer: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 0,
    right: 0,
  },
  input: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    padding: Platform.OS === 'ios' ? 5 : 2,
  },
  zindex: {
    zIndex: 10,
  },
})
