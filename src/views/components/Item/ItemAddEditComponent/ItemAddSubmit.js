import React from 'react'

import { Button } from '../../Button'

export const ItemAddSubmit = ({ categoryId, handleSubmit }) => {
  const getTitle = (id) => {
    switch (id) {
      case 1:
        return 'Add Game'
      case 2:
        return 'Add Book'
      case 3:
        return 'Add Movie'
      case 5:
        return 'Add Music'
      default:
        return 'Add Custom'
    }
  }
  return <Button text={getTitle(categoryId)} handlePress={handleSubmit} />
}
