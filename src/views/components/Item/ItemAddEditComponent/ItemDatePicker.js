import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Dimensions,
  Platform,
} from 'react-native'

import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import { useTheme } from '@react-navigation/native'

export const ItemDatePicker = ({ title, value, onChangeHandler }) => {
  const theme = useTheme()
  const [show, setShow] = useState(false)

  const onChangeDatePicker = (event, selectedDate) => {
    setShow(false)
    onChangeHandler(selectedDate)
  }

  const width = Dimensions.get('window').width / 2

  return (
    <>
      <View style={styles.container}>
        <Text style={{ ...styles.title, ...theme.text }}>{title}:</Text>
        <TouchableWithoutFeedback onPress={() => setShow(true)}>
          <Text
            style={{
              ...styles.date,
              ...theme.text,
              borderColor: theme.text.color,
              width: width,
            }}
          >
            {moment(value).format('DD.MM.YYYY')}
          </Text>
        </TouchableWithoutFeedback>
      </View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={value}
          mode="date"
          display={Platform.OS === 'ios' ? 'inline' : 'default'}
          onChange={onChangeDatePicker}
          style={styles.datePickerIOS}
        />
      )}
    </>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    flex: 1,
    fontSize: 20,
  },
  date: {
    borderWidth: 2,
    fontSize: 15,
    padding: 7,
  },
  datePickerIOS: {
    backgroundColor: 'white',
  },
})
