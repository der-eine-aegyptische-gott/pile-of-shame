import { useTheme } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, TextInput, View, Dimensions } from 'react-native'

export const ItemText = ({ title, value, onChangeHandler, numberOfLines }) => {
  const theme = useTheme()
  return (
    <View style={styles.container}>
      <Text style={{ ...styles.title, ...theme.text }}>{title}:</Text>
      <TextInput
        numberOfLines={numberOfLines}
        value={value}
        multiline={!!numberOfLines}
        onChangeText={onChangeHandler}
        style={{
          ...styles.input,
          ...theme.text,
          width: Dimensions.get('window').width / 2,
          borderColor: theme.text.color,
        }}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    flex: 1,
    fontSize: 20,
  },
  input: {
    borderWidth: 2,
    fontSize: 15,
    padding: 7,
    maxHeight: 300,
  },
})
