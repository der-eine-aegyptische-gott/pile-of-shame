import React, { useState } from 'react'
import { StyleSheet, Image, View } from 'react-native'

import {
  ItemText,
  ItemDatePicker,
  ItemEditSubmit,
  ItemImage,
} from './ItemAddEditComponent'

import Placeholder from '../../../../assets/pile_of_shame_placeholder.png'

export const ItemEdit = ({ item, handleSubmit }) => {
  const [title, setTitle] = useState(item.title)
  const [image, setImage] = useState(
    item.imgPath || Image.resolveAssetSource(Placeholder).uri
  )
  const [description, setDescription] = useState(item.description)
  const [releaseDate, setReleaseDate] = useState(item.releaseDate * 1000)

  return (
    <View style={styles.content}>
      <ItemText
        title="Title"
        value={title}
        onChangeHandler={(e) => setTitle(e)}
      />
      <ItemImage
        title="Image"
        value={image}
        onChangeHandler={(e) => setImage(e)}
      />
      <ItemText
        title="Description"
        value={description}
        numberOfLines={8}
        onChangeHandler={(e) => setDescription(e)}
      />
      <ItemDatePicker
        title="ReleaseDate"
        value={releaseDate}
        onChangeHandler={(e) => setReleaseDate(e)}
      />
      <ItemEditSubmit
        categoryId={item.category.id}
        handleSubmit={() =>
          handleSubmit({
            ...item,
            title,
            description,
            releaseDate : releaseDate/ 1000,
            imgPath : image
          })
        }
      />
    </View>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 10,
  },
})
