import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'

import { ChangeReviewModal, ChangeTagsModal } from '../Modals'

import {
  ItemDetailImage,
  ItemDetailTitle,
  ItemDetailTagRating,
  ItemDetailText,
  ItemDetailBoldText,
} from './ItemDetailComponents'

export const ItemDetail = ({ item, fromView, itemImgPath, tagEditHandler }) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [modalReview, setModalReview] = useState(false)

  const changeTags = () => {
    setModalVisible(true)
  }
  const closeModal = () => {
    setModalVisible(false)
  }
  const reviewModal = () => {
    setModalReview(true)
  }
  const closeReviewModal = () => {
    setModalReview(false)
  }

  return (
    <>
      <ChangeTagsModal
        backdropPress={closeModal}
        visible={modalVisible}
        currentItem={item}
        fromView={fromView}
      />
      <ChangeReviewModal
        backdropPress={closeReviewModal}
        visible={modalReview}
        currentItem={item}
        fromView={fromView}
      />
      <ItemDetailImage imagePath={itemImgPath} />
      <View style={styles.content}>
        <ItemDetailTitle title={item.title} icon={item.category.icon} />
        <ItemDetailTagRating
          tags={item.tags}
          tagChangeHandler={changeTags}
          tagEditHandler={tagEditHandler}
          rating={item.rating}
          ratingEditHandler={reviewModal}
        />
        <ItemDetailText
          text={`Release Date: ${new Date(
            item.releaseDate * 1000
          ).toDateString()}`}
        />
        <ItemDetailBoldText text={item.description} />
        <View style={styles.end}>
          <ItemDetailText
            text={`Created at ${new Date(
              item.createdAt * 1000
            ).toDateString()}`}
          />
        </View>
      </View>
    </>
  )
}
const styles = StyleSheet.create({
  content: {
    margin: 20,
  },
  end: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})
