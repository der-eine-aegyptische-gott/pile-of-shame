import React from 'react'
import { StyleSheet, Text } from 'react-native'

import { useTheme } from '@react-navigation/native'

export const ItemDetailBoldText = ({ text }) => {
  const theme = useTheme()
  return (
    <>
      {(
        <Text style={{ ...styles.boldText, ...theme.text }}>{text}</Text>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  boldText: {
    fontWeight: 'bold',
    marginTop: 15,
  },
})
