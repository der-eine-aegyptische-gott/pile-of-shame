import React from 'react'
import { StyleSheet, View, Text, TouchableWithoutFeedback } from 'react-native'

import { useTheme } from '@react-navigation/native'
import { MaterialCommunityIcons, Entypo, AntDesign } from '@expo/vector-icons'

export const ItemDetailTitle = ({ title, icon }) => {
  const theme = useTheme()
  return (
    <View style={styles.content}>
      <MaterialCommunityIcons
        name={icon}
        size={25}
        style={styles.categoryIcon}
        color={theme.icon.color}
      />
      <Text style={{ ...styles.title, ...theme.text }}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'nowrap',
  },
  categoryIcon: {
    marginRight: 10,
  },
  title: {
    flex: 1,
    fontWeight: 'bold',
    fontSize: 25,
  },
})
