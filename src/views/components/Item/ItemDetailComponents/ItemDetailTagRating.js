import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Rating } from '../../Rating'
import { Tags } from '../../Tags'

export const ItemDetailTagRating = ({
  tags,
  tagChangeHandler,
  tagEditHandler,
  rating,
  ratingEditHandler,
}) => (
  <View style={styles.content}>
    <Tags
      tags={tags}
      changeTagHandler={tagChangeHandler}
      editHandler={tagEditHandler}
    />
    <Rating rating={rating} size={25} editHandler={ratingEditHandler} />
  </View>
)

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
  },
})
