import React from 'react'
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native'

import { useTheme } from '@react-navigation/native'
import { Entypo, AntDesign } from '@expo/vector-icons'

export const ItemDetailButtons = ({ editHandler, deleteHandler }) => {
  const theme = useTheme()
  return (
    <View style={styles.buttonRow}>
      <TouchableWithoutFeedback onPress={editHandler}>
        <Entypo
          name="edit"
          size={25}
          color={theme.icon.color}
          style={styles.editIcon}
        />
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={deleteHandler}>
        <AntDesign name="delete" size={25} color={theme.icon.color} />
      </TouchableWithoutFeedback>
    </View>
  )
}

const styles = StyleSheet.create({
  buttonRow: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    marginTop: 5,
  },
  editIcon: {
    marginRight: 5,
  },
})
