import React from 'react'
import { StyleSheet, View, Image } from 'react-native'

export const ItemDetailImage = ({ imagePath }) => (
  <View>
    <Image source={{ uri: imagePath }} style={styles.image} />
  </View>
)

const styles = StyleSheet.create({
  image: {
    height: 200,
  },
})
