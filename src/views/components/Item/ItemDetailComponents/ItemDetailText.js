import React from 'react'
import { StyleSheet, Text } from 'react-native'

import { useTheme } from '@react-navigation/native'

export const ItemDetailText = ({ text }) => {
  const theme = useTheme()
  return (
    <>{text && <Text style={{ ...styles.text, ...theme.text }}>{text}</Text>}</>
  )
}

const styles = StyleSheet.create({
  text: {
    marginTop: 15,
  },
})
