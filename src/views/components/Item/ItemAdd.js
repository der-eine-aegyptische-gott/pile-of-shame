import React, { useState } from 'react'
import { StyleSheet, Image, View } from 'react-native'
import * as FileSystem from 'expo-file-system'

import {
  ItemText,
  ItemDatePicker,
  ItemAddSearch,
  ItemAddSubmit,
  ItemImage,
} from './ItemAddEditComponent'

import { getImageBySizeType } from '../../../logic/util/helper/itemHelper'
import Placeholder from '../../../../assets/pile_of_shame_placeholder.png'

export const ItemAdd = ({ category, handleSubmit }) => {
  const [title, setTitle] = useState(' test')
  const [image, setImage] = useState(Image.resolveAssetSource(Placeholder).uri)
  const [description, setDescription] = useState('')
  const [releaseDate, setReleaseDate] = useState(new Date())

  const itemSearchHandler = async (item) => {
    if (item.imgPath) {
      let imageUrl = getImageBySizeType(item.imgPath, '720p')
      const downloadResumable = FileSystem.createDownloadResumable(
        imageUrl,
        FileSystem.documentDirectory + '_' + item.id,
        {}
      )
      const imageObject = await downloadResumable.downloadAsync()
      setImage(imageObject.uri)
    }
    setTitle(item.title)
    setDescription(item.description)
    setReleaseDate(item.releaseDate)
  }

  const searchBarCheck = () => {
    return category.id === 1 || category.id;
  }

  return (
    <View style={styles.content}>
      {searchBarCheck && (
        <ItemAddSearch itemSearchHandler={itemSearchHandler} searchCategory={category}/>
      )}
      <ItemText
        title="Title"
        value={title}
        onChangeHandler={(e) => setTitle(e)}
      />
      <ItemImage
        title="Image"
        value={image}
        onChangeHandler={(e) => setImage(e)}
      />
      <ItemText
        title="Description"
        value={description}
        numberOfLines={8}
        onChangeHandler={(e) => setDescription(e)}
      />
      <ItemDatePicker
        title="ReleaseDate"
        value={releaseDate}
        onChangeHandler={(e) => setReleaseDate(e)}
      />
      <ItemAddSubmit
        categoryId={category.id}
        handleSubmit={() =>
          handleSubmit({
            category,
            title,
            image,
            description,
            releaseDate,
          })
        }
      />
    </View>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 10,
  },
})
