import React from 'react'
import { StyleSheet } from 'react-native'

import {
  SettingSwitch,
  SettingHeader,
  SettingPicker,
  SettingImportExport,
} from './SettingsComponent'

export const Settings = ({
  theme,
  saveTheme,
  homeMostRecent,
  saveHomeMostRecent,
  homeTag,
  saveHomeTag,
  tags,
  importHandler,
  exportHandler,
}) => {
  const themes = [
    { label: 'System Default', value: 'default' },
    { label: 'Light', value: 'light' },
    { label: 'Dark', value: 'dark' },
    { label: 'Red', value: 'red' },
  ]

  return (
    <>
      <SettingHeader title="Design" />
      <SettingPicker
        title="Theme"
        modalTitle="Change Theme"
        value={theme}
        values={themes}
        onValueChange={saveTheme}
      />
      <SettingHeader title="Home" />
      <SettingSwitch
        title="Enable MostRecent"
        value={homeMostRecent}
        onValueChange={() => saveHomeMostRecent(!homeMostRecent)}
      />
      <SettingPicker
        title="HomeTag"
        modalTitle="Change HomeTag"
        value={(homeTag && homeTag.name) || 'none'}
        values={[
          ...tags.map((tag) => ({ label: tag.name, value: tag })),
          { label: 'None', value: {} },
        ]}
        onValueChange={saveHomeTag}
      />
      <SettingImportExport
        importHandler={importHandler}
        exportHandler={exportHandler}
      />
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  text: {
    fontSize: 20,
  },
})
