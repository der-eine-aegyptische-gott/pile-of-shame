import React, { useState } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { useTheme } from '@react-navigation/native'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'

import { SettingPickerModal } from '../../Modals'

export const SettingPicker = ({
  title,
  modalTitle,
  value,
  values,
  onValueChange,
}) => {
  const theme = useTheme()
  const [isModalOpen, setIsModalOpen] = useState(false)
  return (
    <>
      <SettingPickerModal
        title={modalTitle}
        values={values}
        onPressHandler={onValueChange}
        visible={isModalOpen}
        closeModalHandler={() => setIsModalOpen(false)}
      />
      <View style={styles.container}>
        <Text style={{ ...styles.text, ...theme.text }}>{title}</Text>
        <TouchableWithoutFeedback onPress={() => setIsModalOpen(true)}>
          <Text style={{ ...styles.text, ...theme.text }}>{value}</Text>
        </TouchableWithoutFeedback>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  text: {
    fontSize: 20,
  },
})
