import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const SettingHeader = ({ title }) => {
  const theme = useTheme()
  return (
    <View style={styles.container}>
      <Text style={{ ...styles.title, ...theme.title }}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    margin: 10,
  },
})
