import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Button } from '../../Button'

export const SettingImportExport = ({ importHandler, exportHandler }) => {
  return (
    <View style={styles.container}>
      <Button
        text="Export Database"
        handlePress={exportHandler}
        styles={styles.button}
      />
      <Button text="Import Database" handlePress={importHandler} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  button: {
    marginBottom: 20,
    marginTop: 20,
  },
})
