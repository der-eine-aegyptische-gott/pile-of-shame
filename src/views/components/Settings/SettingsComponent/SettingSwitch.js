import React from 'react'
import { Switch, StyleSheet, View, Text } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const SettingSwitch = ({ title, value, onValueChange }) => {
  const theme = useTheme()
  return (
    <View style={styles.container}>
      <Text style={{ ...styles.text, ...theme.text }}>{title}</Text>
      <Switch
        trackColor={theme.switch.trackColor}
        thumbColor={value ? '#f5dd4b' : '#f4f3f4'}
        ios_backgroundColor={theme.switch.iosBackgroundColor}
        onValueChange={onValueChange}
        value={value}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 10,
  },
  text: {
    fontSize: 20,
  },
})
