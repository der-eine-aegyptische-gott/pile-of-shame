import React, { useState } from 'react'
import { StyleSheet, Image, View, TextInput } from 'react-native'
import { useTheme } from '@react-navigation/native'

import { FontAwesome } from '@expo/vector-icons'

import AppImage from '../../../assets/icon_alpha.png'
import { Header } from './Header'

export const SearchBar = ({ setSearchQuery, text, searchQuery }) => {
  const { textInput, textInputPlaceholderColor } = useTheme()
  const [appImage] = useState(Image.resolveAssetSource(AppImage).uri)
  return (
    <Header>
      <Image source={{ uri: appImage }} style={{ width: 55, height: 55 }} />
      <View style={styles.searchBar}>
        <FontAwesome
          name="search"
          size={20}
          color="white"
          style={styles.icon}
        />
        <TextInput
          value={searchQuery}
          placeholder={text}
          placeholderTextColor={textInputPlaceholderColor}
          style={textInput}
          onChangeText={(searchQuery) => setSearchQuery(searchQuery)}
        />
      </View>
    </Header>
  )
}
const styles = StyleSheet.create({
  searchBar: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
  },
  icon: {
    marginRight: 5,
  },
})
