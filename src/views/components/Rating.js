import React from 'react'
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native'
import { useTheme } from '@react-navigation/native'

import { FontAwesome } from '@expo/vector-icons'

export const Rating = ({ rating, size, editHandler }) => {
  const theme = useTheme()
  const amount = 5
  const fullStars = Math.floor(rating / 2)
  const halfRating = (rating / 2) % 1
  const editFunction = editHandler ? () => editHandler() : null
  return (
    <TouchableWithoutFeedback onPress={editFunction}>
      <View style={styles.container}>
        {[...Array(amount)].map((e, i) =>
          i === fullStars && halfRating !== 0 ? (
            <FontAwesome
              key={i}
              name="star-half-full"
              size={size}
              color={theme.rating.color}
            />
          ) : i < fullStars ? (
            <FontAwesome
              key={i}
              name="star"
              size={size}
              color={theme.rating.color}
            />
          ) : (
            <FontAwesome
              key={i}
              name="star-o"
              size={size}
              color={theme.rating.color}
            />
          )
        )}
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: { flexDirection: 'row' },
})
