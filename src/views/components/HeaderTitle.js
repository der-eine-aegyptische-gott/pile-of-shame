import React, { useState } from 'react'
import { Image, StyleSheet, Text } from 'react-native'
import { useTheme } from '@react-navigation/native'

import { Header } from './Header'
import AppImage from '../../../assets/icon_alpha.png'

export const HeaderTitle = ({ text }) => {
  const { title } = useTheme()
  const [appImage] = useState(Image.resolveAssetSource(AppImage).uri)
  return (
    <Header>
      <Image style={styles.image} source={{ uri: appImage }} />
      <Text style={{ ...title, ...styles.title }}>{text}</Text>
    </Header>
  )
}

const styles = StyleSheet.create({
  image: {
    width: 55,
    height: 55,
  },
  title: {
    margin: 10,
    marginLeft: 5,
  },
})
