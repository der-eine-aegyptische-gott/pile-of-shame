import React from 'react'
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native'

import { AntDesign } from '@expo/vector-icons'
import { useTheme } from '@react-navigation/native'

import { Header } from './Header'

export const DetailHeader = ({ onIconPressHandler, title, buttons }) => {
  const { icon, text } = useTheme()
  return (
    <Header>
      <TouchableWithoutFeedback onPress={onIconPressHandler}>
        <AntDesign
          name="caretleft"
          size={30}
          style={{ ...styles.icon, ...icon }}
        />
      </TouchableWithoutFeedback>
      <View style={styles.container}>
        <Text style={{ ...styles.text, ...text }}>{title}</Text>
        {buttons}
      </View>
    </Header>
  )
}

const styles = StyleSheet.create({
  icon: { fontSize: 35, marginLeft: 20, margin: 10 },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: 20,
  },
  text: {
    fontSize: 20,
  },
})
