import React from 'react'
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native'

import { Tag } from '../../Tags'

export const ChangeTagsModalTagButton = ({ dataTag, onPressHandler }) => {
  const theme = useTheme()
  return (
    <TouchableWithoutFeedback onPress={onPressHandler}>
      <View style={styles.content}>
        <Tag tag={dataTag.tag} editHandler={onPressHandler} doubleSize />
        <Text style={{ ...styles.checked, ...theme.text }}>
          {dataTag.active ? '✓' : '-'}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  )
}
const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 5,
    marginBottom: 5,
    justifyContent: 'space-between',
  },
  checked: {
    marginLeft: 20,
    fontSize: 25,
    fontWeight: 'bold',
  },
})
