import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Modal from 'react-native-modal'
import { useTheme } from '@react-navigation/native'

import { Button } from '../Button'

import { SettingPickerModalButton } from './SettingPickerModalComponents'

export const SettingPickerModal = ({
  title,
  values,
  onPressHandler,
  visible,
  closeModalHandler,
}) => {
  const theme = useTheme()

  return (
    <Modal
      backdropOpacity={0.3}
      isVisible={visible}
      onBackdropPress={() => closeModalHandler()}
    >
      <View style={{ ...styles.modal, ...theme.modal }}>
        <Text style={{ ...theme.title, ...styles.title }}>{title}</Text>
        <View style={styles.tags}>
          {values.map((value, index) => (
            <SettingPickerModalButton
              key={index}
              value={value.label}
              onPressHandler={() => {
                onPressHandler(value.value)
                closeModalHandler()
              }}
            />
          ))}
        </View>
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  modal: {
    padding: 22,
    borderRadius: 4,
    borderWidth: 5,
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    marginBottom: 12,
    textAlign: 'center',
  },
  tags: {
    marginBottom: 10,
  },
})
