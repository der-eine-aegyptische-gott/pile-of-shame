import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'
import Modal from 'react-native-modal'
import { useTheme } from '@react-navigation/native'
import { useNavigation } from '@react-navigation/native'
import { EvilIcons } from '@expo/vector-icons'

import { selectCategories } from '../../../logic/CategoryRepository'

import { AddScreenModalCategoryButton } from './AddScreenModalComponent'

export const AddScreenModal = () => {
  const theme = useTheme()
  const [categorys, setCatgorys] = useState([])
  const [modalVisible, setModalVisible] = useState(false)
  const navigation = useNavigation()

  useEffect(() => {
    selectCategories().then((value) => {
      setCatgorys(value)
    })
  }, [])

  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible(true)
        }}
      >
        <View
          style={{
            backgroundColor: 'transparent',
            elevation: 0,
            paddingTop: 0,
            paddingBottom: 0,
          }}
        >
          <EvilIcons
            name="plus"
            size={60}
            color={theme.navigation.inactiveTintColor}
            style={{ marginBottom: -3, fontSize: 60 }}
          />
        </View>
      </TouchableWithoutFeedback>
      <View>
        <Modal
          backdropOpacity={0.3}
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
        >
          <View style={{ ...styles.modal, ...theme.modal }}>
            <Text style={{ ...styles.contentTitle, ...theme.title }}>
              Create Item
            </Text>
            {categorys.map((category, index) => (
              <AddScreenModalCategoryButton
                key={index}
                name={category.name}
                icon={category.icon}
                onPressHandler={() => {
                  navigation.navigate('ItemAdd', {
                    category: category.id,
                  })
                  setModalVisible(false)
                }}
              />
            ))}
            <AddScreenModalCategoryButton
              name="Tag"
              icon="alpha-t-box"
              onPressHandler={() => {
                navigation.navigate('TagAdd')
                setModalVisible(false)
              }}
            />
          </View>
        </Modal>
      </View>
    </>
  )
}
const styles = StyleSheet.create({
  modal: {
    padding: 22,
    borderRadius: 4,
    borderWidth: 5,
  },
  contentTitle: {
    marginBottom: 12,
    textAlign: 'center',
  },
  contentView: {
    justifyContent: 'flex-end',
    margin: 0,
  },
})
