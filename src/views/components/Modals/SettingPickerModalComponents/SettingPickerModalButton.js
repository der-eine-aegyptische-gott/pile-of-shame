import React from 'react'
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const SettingPickerModalButton = ({ value, onPressHandler }) => {
  const theme = useTheme()
  return (
    <TouchableWithoutFeedback onPress={onPressHandler}>
      <View style={{ ...styles.content, ...theme.button }}>
        <Text style={{ ...styles.text, ...theme.text }}>{value}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}
const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    marginRight: 5,
    marginBottom: 5,
    paddingLeft: 20,
    borderRadius: 5,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'center',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
  },
})
