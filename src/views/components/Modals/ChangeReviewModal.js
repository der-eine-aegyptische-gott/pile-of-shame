import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, TextInput } from 'react-native'
import Modal from 'react-native-modal'
import { useTheme } from '@react-navigation/native'
import { useNavigation } from '@react-navigation/native'
import Slider from '@react-native-community/slider'

import { updateItemObject } from '../../../logic/ItemRepository'

import { Button } from '../Button'
import { Rating } from '../Rating'

export const ChangeReviewModal = (props) => {
  const theme = useTheme()
  const navigation = useNavigation()
  const [sliderValue, setSliderValue] = useState(props.currentItem.rating)
  const [reviewValue, setReviewValue] = useState(props.currentItem.review)

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {})
    return unsubscribe
  }, [])

  const handleSubmit = () => {
    let currentItem = props.currentItem
    currentItem.review = reviewValue
    currentItem.rating = sliderValue
    updateItemObject(currentItem).then(() => {
      props.backdropPress()
    })
  }

  return (
    <Modal
      backdropOpacity={0.3}
      isVisible={props.visible}
      onBackdropPress={() => props.backdropPress()}
    >
      <View style={{ ...styles.content, ...theme.modal }}>
        <Text style={{ ...theme.title, ...styles.contentTitle }}>
          Change Review
        </Text>
        <View>
          <Slider
            style={{
              width: 240,
              height: 50,
              opacity: 0.011,
              position: 'absolute',
              zIndex: 100,
            }}
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor="#222222"
            maximumTrackTintColor="#000000"
            step={1}
            value={sliderValue}
            onValueChange={(value) => setSliderValue(value)}
          />
          <Rating rating={sliderValue} size={50} />
        </View>
        <TextInput
          style={{ ...styles.textInput, ...theme.textInput }}
          placeholderTextColor={theme.textInputPlaceholderColor}
          multiline
          rowSpan={6}
          placeholder="Textarea"
          value={reviewValue}
          onChangeText={(value) => setReviewValue(value)}
        />
        <Button text="Save" handlePress={() => handleSubmit()} />
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 22,
    borderRadius: 4,
    borderWidth: 5,
    alignItems: 'center',
  },
  contentTitle: {
    fontSize: 40,
    marginBottom: 12,
  },
  contentView: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  textInput: {
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: 'white',
    padding: 5,
    marginBottom: 10,
    marginTop: 10,
  },
  button: {
    marginTop: 5,
  },
})
