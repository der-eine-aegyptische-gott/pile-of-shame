import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, TextInput } from 'react-native'
import Modal from 'react-native-modal'
import { useTheme } from '@react-navigation/native'
import { useNavigation } from '@react-navigation/native'
import { Button } from '../Button'

export const DeleteModal = ({
  title,
  currentItem,
  deleteHandler,
  closeModalHandler,
  visible,
}) => {
  const theme = useTheme()
  const navigation = useNavigation()

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {})
    return unsubscribe
  }, [])

  const handleSubmitDelete = () => {
    deleteHandler(currentItem.id)
    closeModalHandler()
  }

  const handleSubmitCancel = () => {
    closeModalHandler()
  }

  return (
    <Modal
      backdropOpacity={0.3}
      isVisible={visible}
      onBackdropPress={() => closeModalHandler()}
    >
      <View style={{ ...styles.content, ...theme.modal }}>
        <Text style={{ ...styles.title, ...theme.text }}>{title}</Text>
        <View style={styles.buttonRow}>
          <Button text="Cancel " handlePress={() => handleSubmitCancel()} />
          <Button text="Delete" handlePress={() => handleSubmitDelete()} />
        </View>
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 22,
    borderRadius: 4,
    borderWidth: 5,
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 12,
  },
  buttonRow: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'space-around',
  },
})
