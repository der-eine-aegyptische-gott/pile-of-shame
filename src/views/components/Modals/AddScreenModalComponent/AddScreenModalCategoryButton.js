import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'
import { useTheme } from '@react-navigation/native'
import { MaterialCommunityIcons } from '@expo/vector-icons'

export const AddScreenModalCategoryButton = ({
  onPressHandler,
  icon,
  name,
}) => {
  const theme = useTheme()

  return (
    <TouchableWithoutFeedback onPress={onPressHandler}>
      <View style={styles.content}>
        <MaterialCommunityIcons
          name={icon}
          size={30}
          style={{ ...styles.icon, ...theme.icon }}
        />
        <Text style={{ ...theme.title, ...styles.text }}>{name}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}
const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    // borderWidth: 1,
    marginTop: 5,
  },
  icon: {
    marginRight: 5,
  },
  text: {
    fontSize: 20,
    // font,
  },
})
