import React, { useEffect, useState } from 'react'
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native'
import Modal from 'react-native-modal'
import { useNavigation, useTheme } from '@react-navigation/native'

import { selectTags } from '../../../logic/TagRepository'
import { updateTagsFromItem } from '../../../logic/ItemRepository'

import { Button } from '../Button'

import { ChangeTagsModalTagButton } from './ChangeTagsModalComponents'

export const ChangeTagsModal = (props) => {
  const theme = useTheme()
  const navigation = useNavigation()
  const [selectedTags, setSelectedTags] = useState([])
  let newTags = []
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      selectTags().then((data) => {
        let newTags = [];
        data.map((_tag) => newTags.push({ tag: _tag, active: false }))
        if (props.currentItem.tags.length > 0) {
          props.currentItem.tags.forEach((tag) => {
            let index = newTags.findIndex(
              (dataTag) => dataTag.tag.id === tag.id
            )
            if (index >= 0) {
              newTags[index].active = true
            }
          })
        }
        setSelectedTags(newTags)
      })
    })
    return unsubscribe
  }, [])

  const selectTag = ($tag) => {
    let Items = [...selectedTags]
    let found = Items.findIndex((data) => data.tag.id === $tag.tag.id)
    if (found >= 0) {
      Items[found].active = !Items[found].active
    }
    setSelectedTags(Items)
  }
  const handleSubmit = () => {
    if (selectedTags) {
      let activeTags = selectedTags.filter((data) => data.active === true)
      let toSaveTags = activeTags.map((tag) => tag.tag)
      updateTagsFromItem(props.currentItem, toSaveTags).then(() => {
        navigation.navigate(props.fromView)
      })
    }
  }

  return (
    <Modal
      backdropOpacity={0.3}
      isVisible={props.visible}
      onBackdropPress={() => props.backdropPress()}
    >
      <View style={{ ...styles.modal, ...theme.modal }}>
        <Text style={{ ...theme.title, ...styles.title }}>Change Tags</Text>
        <View style={styles.tags}>
          {selectedTags.map((dataTag, index) => (
            <ChangeTagsModalTagButton
              key={index}
              dataTag={dataTag}
              onPressHandler={() => selectTag(dataTag)}
            />
          ))}
        </View>
        <Button text="Save" handlePress={() => handleSubmit()} />
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  modal: {
    padding: 22,
    borderRadius: 4,
    borderWidth: 5,
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    marginBottom: 12,
    textAlign: 'center',
  },
  tags: {
    marginBottom: 10,
  },
})
