import React, { useState } from 'react'
import { Image, StyleSheet, View, TouchableWithoutFeedback } from 'react-native'
import { useTheme } from '@react-navigation/native'

import Placeholder from '../../../../assets/pile_of_shame_placeholder.png'

import { ChangeReviewModal } from '../Modals'

import { ItemCardBody } from './ItemCardComponents/ItemCardBody'

export const ItemCard = ({ item, onPressHandler, searchQuery }) => {
  const { itemCard } = useTheme()
  const [modalReview, setModalReview] = useState(false)
  const reviewModal = () => {
    setModalReview(true)
  }
  const closeReviewModal = () => {
    setModalReview(false)
  }
  const placeholderImage = Image.resolveAssetSource(Placeholder).uri
  return (
    <>
      <ChangeReviewModal
        backdropPress={closeReviewModal}
        visible={modalReview}
        currentItem={item}
      />
      <TouchableWithoutFeedback onPress={() => onPressHandler(item)}>
        <View style={{ ...styles.itemCard, ...itemCard }}>
          <Image
            source={{ uri: item.imgPath || placeholderImage }}
            style={styles.thumbnail}
          />
          <ItemCardBody
            item={item}
            tagEditHandler={() => onPressHandler(item)}
            searchQuery={searchQuery}
            reviewModal={reviewModal}
          />
        </View>
      </TouchableWithoutFeedback>
    </>
  )
}

const styles = StyleSheet.create({
  itemCard: {
    flexDirection: 'row',
    borderRadius: 7,
    padding: 8,
    marginBottom: 10,
  },
  thumbnail: { width: 80, height: 80 },
})
