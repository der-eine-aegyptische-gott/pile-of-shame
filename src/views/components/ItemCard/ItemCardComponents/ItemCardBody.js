import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Rating } from '../../Rating'
import { Tags } from '../../Tags'

import { ItemCardTitle } from './ItemCardTitle'

export const ItemCardBody = ({
  item,
  searchQuery,
  reviewModal,
  tagEditHandler,
}) => {
  const { title, tags, rating } = item
  return (
    <View style={styles.itemCardBody}>
      <ItemCardTitle title={title} search={searchQuery} />
      <View style={styles.itemCardRow}>
        <Tags tags={tags} editHandler={tagEditHandler} />
        <Rating rating={rating || 0} size={20} editHandler={reviewModal} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  itemCardBody: {
    paddingLeft: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderRadius: 1,
    flex: 1,
  },
  itemCardRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
})
