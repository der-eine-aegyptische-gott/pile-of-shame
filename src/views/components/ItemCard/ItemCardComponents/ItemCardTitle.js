import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { useTheme } from '@react-navigation/native'

export const ItemCardTitle = ({ title, search }) => {
  const { text, marked } = useTheme()
  if (!search) {
    return <Text style={{ ...styles.text, ...text }}>{title}</Text>
  }

  const position = title.toLowerCase().indexOf(search.toLowerCase())
  const start = title.slice(0, position)
  const middle = title.slice(position, position + search.length)
  const end = title.slice(position + search.length, title.length)
  return (
    <Text>
      <Text style={{ ...styles.text, ...text }}>{start}</Text>
      <Text style={{ ...styles.text, ...marked }}>{middle}</Text>
      <Text style={{ ...styles.text, ...text }}>{end}</Text>
    </Text>
  )
}

const styles = StyleSheet.create({
  text: {
    fontWeight: 'bold',
    fontSize: 18,
  },
})
