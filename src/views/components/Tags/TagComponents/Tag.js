import React from 'react'
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native'

export const Tag = ({ tag, editHandler, doubleSize }) => {
  const editFunction = editHandler ? () => editHandler(tag) : null
  const size = doubleSize ? 20 : 15
  return (
    <TouchableWithoutFeedback onPress={editFunction}>
      <View
        style={{
          backgroundColor: tag.colorcode,
          ...styles.tag,
        }}
      >
        <Text style={{ ...styles.tagText, fontSize: size }}>{tag.name}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  tag: {
    marginRight: 5,
    marginTop: 5,
    borderRadius: 4,
    borderColor: 'transparent',
    padding: 2,
    paddingLeft: 7,
    paddingRight: 7,
  },
  tagText: {
    fontWeight: 'bold',
    color: 'white',
  },
})
