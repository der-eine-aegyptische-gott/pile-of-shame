import React from 'react'
import { StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import { useTheme } from '@react-navigation/native'

export const TagChangeButton = ({ changeTagHandler }) => {
  const { icon } = useTheme()
  return (
    <TouchableWithoutFeedback onPress={() => changeTagHandler()}>
      <Entypo
        name="squared-plus"
        size={25}
        color={icon.color}
        style={styles.button}
      />
    </TouchableWithoutFeedback>
  )
}
const styles = StyleSheet.create({
  button: {
    marginRight: 5,
  },
})
