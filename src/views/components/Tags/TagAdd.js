import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'

import { Button } from '../Button'

import {
  TagAddColorPicker,
  TagAddText,
  TagAddSubmit,
} from './TagAddEditComponents'

export const TagAdd = ({ handleSubmit }) => {
  const [name, setName] = useState('')
  const [tooltip, setToolTip] = useState('')
  const [color, setColor] = useState('#FF0000')

  return (
    <View style={styles.content}>
      <TagAddText
        title="Name"
        value={name}
        onChangeHandler={(value) => setName(value)}
      />
      <TagAddText
        title="Description"
        value={tooltip}
        onChangeHandler={(value) => setToolTip(value)}
      />
      <TagAddColorPicker value={color} onChangeHandler={(e) => setColor(e)} />
      <Button
        text="Add Tag"
        handlePress={() => handleSubmit({ name, tooltip, color })}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 10,
  },
})
