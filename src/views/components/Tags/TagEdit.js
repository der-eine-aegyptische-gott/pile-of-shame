import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'

import { Button } from '../Button'

import {
  TagAddColorPicker,
  TagAddText,
  TagEditSubmit,
} from './TagAddEditComponents'

export const TagEdit = ({ tag, handleSubmit }) => {
  const [name, setName] = useState(tag.name)
  const [tooltip, setToolTip] = useState(tag.tooltip)
  const [colorcode, setColorcode] = useState(tag.colorcode)

  return (
    <View style={styles.content}>
      <TagAddText
        title="Name"
        value={name}
        onChangeHandler={(value) => setName(value)}
      />
      <TagAddText
        title="Description"
        value={tooltip}
        onChangeHandler={(value) => setToolTip(value)}
      />
      <TagAddColorPicker
        value={colorcode}
        onChangeHandler={(e) => setColorcode(e)}
      />
      <Button
        text="Edit Tag"
        handlePress={() => handleSubmit({ ...tag, name, tooltip, colorcode })}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  content: {
    padding: 10,
  },
})
