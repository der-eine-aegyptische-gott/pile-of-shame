import React from 'react'
import { StyleSheet, View } from 'react-native'

import { Tag, TagChangeButton } from './TagComponents'

export const Tags = ({ tags, editHandler, changeTagHandler }) => (
  <View style={styles.container}>
    {tags.map((tag, index) => {
      return <Tag key={index} tag={tag} editHandler={editHandler} />
    })}
    {changeTagHandler && (
      <TagChangeButton changeTagHandler={changeTagHandler} />
    )}
  </View>
)
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    flex: 1,
  },
})
