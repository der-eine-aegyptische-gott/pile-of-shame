import React from 'react'
import { Dimensions } from 'react-native'
import { ColorPicker, fromHsv } from 'react-native-color-picker'

export const TagAddColorPicker = ({ value, onChangeHandler }) => {
  const height = Dimensions.get('window').height / 2.5
  return (
    <ColorPicker
      defaultColor={value}
      onColorChange={(color) => onChangeHandler(fromHsv(color))}
      style={{ height: height }}
    />
  )
}
