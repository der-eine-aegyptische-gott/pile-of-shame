import { useTheme } from '@react-navigation/native'
import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'

export const TagAddText = ({
  title,
  value,
  onChangeHandler,
  numberOfLines,
}) => {
  const theme = useTheme()
  return (
    <View style={styles.container}>
      <Text style={{ ...styles.title, ...theme.text }}>{title}:</Text>
      <TextInput
        numberOfLines={numberOfLines}
        value={value}
        multiline={!!numberOfLines}
        onChangeText={onChangeHandler}
        style={{
          ...styles.input,
          ...theme.text,
          borderColor: theme.text.color,
        }}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    flex: 1,
    color: 'white',
    fontSize: 20,
  },
  input: {
    borderColor: 'white',
    color: 'white',
    borderWidth: 2,
    fontSize: 15,
    padding: 7,
    width: 225,
  },
})
