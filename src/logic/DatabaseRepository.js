import * as FileSystem from 'expo-file-system'
import * as Sharing from 'expo-sharing'
import * as DocumentPicker from 'expo-document-picker'
import * as SQLite from 'expo-sqlite'
import {Asset} from "expo-asset";
const db = SQLite.openDatabase('pileofshame.db');
const databaseFolder = `${FileSystem.documentDirectory}/SQLite`
const databasePath = `${databaseFolder}/pileofshame.db`

export const exportDataBase = async () => {
  const { uri } = await FileSystem.getInfoAsync(databasePath)
  Sharing.shareAsync(uri, { mimeType: 'application/x-sqlite3' })
}

export const importDataBase = async () => {
  DocumentPicker.getDocumentAsync({
    mimeType: 'application/x-sqlite3',
  }).then(async (file) => {
    await db._db.close();
    await FileSystem.deleteAsync(FileSystem.documentDirectory + 'SQLite/pileofshame.db');
    if (!(await FileSystem.getInfoAsync(FileSystem.documentDirectory + 'SQLite')).exists) {
      await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + 'SQLite');
    }
    await FileSystem.copyAsync({
      from:  Asset.fromModule(file.uri.toString()).uri,
      to: FileSystem.documentDirectory + 'SQLite/pileofshame.db'
    });
    return SQLite.openDatabase('pileofshame.db')
  })
}
