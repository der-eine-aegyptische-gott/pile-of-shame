import { executeQuery } from '../database/accessor'
import { Tag } from './model/Tag'

export const addTag = async (_name, _tooltip, _colorcode) => {
  const insert = await executeQuery(
    `INSERT INTO tags(name, tooltip, colorcode) 
    VALUES(?,?,?)`,
    [_name, _tooltip, _colorcode]
  )
  if (insert.insertId) {
    return new Tag(insert.insertId, _name, _tooltip, _colorcode)
  } else {
    return null
  }
}

export const selectTags = async () => {
  const select = await executeQuery(`SELECT * FROM tags`)
  return select.rows._array.map(
    (item) => new Tag(item.id, item.name, item.tooltip, item.colorcode)
  )
}

export const selectTagsByProperty = async (_property, _value) => {
  const select = await executeQuery(
    `SELECT * FROM tags WHERE ${_property} = ${_value}`
  )
  return select.rows._array.map(
    (item) => new Tag(item.id, item.name, item.tooltip, item.colorcode)
  )
}

export const selectTagById = async (_value) => {
  const select = await executeQuery(`SELECT * FROM tags WHERE id = ?`, [_value])
  return select.rows._array
    .map((item) => new Tag(item.id, item.name, item.tooltip, item.colorcode))
    .pop()
}

export const deleteTagById = async (_value) => {
  let deleted = await executeQuery(`DELETE FROM tagsToItems WHERE tag = ?`, [
    _value,
  ])
  deleted += await executeQuery(`DELETE FROM tags WHERE id = ?`, [_value])

  return deleted.rowsAffected !== 0
}

export const updateTagObject = async (_value) => {
  const updated = await executeQuery(
    `Update tags SET name = ?, tooltip = ? , colorcode = ? where id = ?`,
    [_value.name, _value.tooltip, _value.colorcode, _value.id]
  )
  return updated.rowsAffected !== 0
}
