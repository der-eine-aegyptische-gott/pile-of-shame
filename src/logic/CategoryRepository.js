import { executeQuery } from '../database/accessor'
import { Category } from './model/Category'
import { Tag } from './model/Tag'

export const addCategory = async (_name, _icon) => {
  const insert = await executeQuery(
    `INSERT INTO category(name, icon) 
    VALUES(?,?,?)`,
    [_name, _icon]
  )
  if (insert.insertId) {
    return new Category(insert.insertId, _name, _icon)
  } else {
    return null
  }
}

export const selectCategories = async () => {
  const select = await executeQuery(`SELECT * FROM category`)
  return select.rows._array.map(
    (item) => new Category(item.id, item.name, item.icon)
  )
}

export const selectCategoriesByProperty = async (_property, _value) => {
  const select = await executeQuery(
    `SELECT * FROM category WHERE ${_property} = ${_value}`
  )
  return select.rows._array.map(
    (item) => new Category(item.id, item.name, item.icon)
  )
}

export const selectCategoryById = async (_value) => {
  const select = await executeQuery(`SELECT * FROM category WHERE id = ?`, [
    _value,
  ])
  return select.rows._array
    .map((item) => new Category(item.id, item.name, item.icon))
    .pop()
}

export const deleteCategoryById = async (_value) => {
  const deleted = await executeQuery(`DELETE FROM category WHERE id = ?`, [
    _value,
  ])
  return deleted.rowsAffected !== 0
}

export const updateCategoryObject = async (_value) => {
  const updated = await executeQuery(
    `Update category SET name = ?, icon = ? where id = ?`,
    [_value.name, _value.icon, _value.id]
  )
  return updated.rowsAffected !== 0
}
