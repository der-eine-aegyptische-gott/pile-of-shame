export function Category(_id, _name, _icon, _createdAt = '') {
  this.id = _id;
  this.name = _name;
  this.icon = _icon;
}
