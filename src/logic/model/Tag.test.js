import { Tag } from './Tag';

test('Creates a new Tag Object', () => {
  const tagTest = new Tag('', 'Tag Test', 'Test', '#0000');
  expect(tagTest.name).toBe('Tag Test');
  expect(tagTest.colorcode).toBe('#0000');
  expect(tagTest.tooltip).toBe('Test');
});
