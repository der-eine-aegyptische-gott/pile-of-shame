export function Item(
  _id,
  _title,
  _description,
  _category,
  _rating,
  _review,
  _imgPath,
  _releaseDate,
  _tags = [],
  _createdAt= '',
) {
  this.id = _id;
  this.title = _title;
  this.description = _description;
  this.category = _category;
  this.rating = _rating;
  this.review = _review;
  this.imgPath = _imgPath;
  this.releaseDate = _releaseDate;
  this.tags = _tags;
  this.createdAt = _createdAt;
}
