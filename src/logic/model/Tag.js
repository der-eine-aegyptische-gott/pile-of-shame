export function Tag(_id, _name, _tooltip, _colorcode, _createdAt= '') {
  this.id = _id;
  this.name = _name;
  this.tooltip = _tooltip;
  this.colorcode = _colorcode;
}
