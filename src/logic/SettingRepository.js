import { executeQuery } from '../database/accessor'
import { Setting } from './model/Setting'
import { Tag } from './model/Tag'

export const selectSetting = async () => {
  const select = await executeQuery(
    `SELECT settings.id as setting_id, settings.theme, settings.mostrecent, Tags.id as tag_id, Tags.name, Tags.tooltip, Tags.colorcode FROM settings LEFT JOIN Tags ON settings.tag = Tags.id`
  )
  return select.rows._array.map(
    (item) =>
      new Setting(
        item.setting_id,
        item.theme,
        item.mostrecent,
        new Tag(item.tag_id, item.name, item.tooltip, item.colorcode)
      )
  )[0]
}

export const updateSettingObject = async (_value) => {
  const updated = await executeQuery(
    `Update settings SET theme = ?, mostrecent = ?, tag = ? where id = ?`,
    [_value.theme, _value.mostrecent, _value.tag.id, _value.id]
  )
  return updated.rowsAffected !== 0
}
