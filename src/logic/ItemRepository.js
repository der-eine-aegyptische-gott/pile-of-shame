import { executeQuery } from '../database/accessor'
import { Item } from './model/Item'
import { Category } from './model/Category'
import { Tag } from './model/Tag'

export const addItem = async (
  _title,
  _description,
  _category,
  _rating,
  _review,
  _imgPath,
  _releaseDate,
  _tags
) => {
  const insert = await executeQuery(
    `INSERT INTO Items(title, description, category, rating, review, imgPath, releaseDate, createdAt)
    VALUES(?,?,?,?,?,?,?,?)`,
    [
      _title,
      _description,
      _category.id,
      _rating,
      _review,
      _imgPath,
      _releaseDate,
      Date.now() / 1000,
    ]
  )
  if (insert.insertId) {
    if (Array.isArray(_tags)) {
      _tags.map(async (item) => {
        await executeQuery(`INSERT INTO tagsToItems(item, tag) VALUES(?, ?)`, [
          insert.insertId,
          item.id,
        ])
      })
    }
    return new Item(
      insert.insertId,
      _title,
      _description,
      _category,
      _rating,
      _review,
      _imgPath,
      _releaseDate
    )
  } else {
    return null
  }
}

export const selectItems = async () => {
  const select = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id ORDER BY Category.name`
  )
  return getItemWithTags(select.rows._array)
}

export const selectItemsByProperty = async (_property, _value) => {
  const select = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id ORDER BY Category.name WHERE ${_property} = ${_value}`
  )
  return getItemWithTags(select.rows._array)
}

export const selectItemById = async (_value) => {
  const select = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id ORDER BY Category.name WHERE Items.id = ?`,
    [_value]
  )
  return getItemWithTags(select.rows._array).pop()
}

export const deleteItemById = async (_value) => {
  await executeQuery(`DELETE FROM tagsToItems WHERE item = ?`, [_value])
  const deleted = await executeQuery(`DELETE FROM Items WHERE id = ?`, [_value])
  return deleted.rowsAffected !== 0
}

export const updateItemObject = async (_value) => {
  const updated = await executeQuery(
    `Update Items SET title = ?, description = ?, rating = ?, review = ?, imgPath = ?, releaseDate = ? where id = ?`,
    [
      _value.title,
      _value.description,
      _value.rating,
      _value.review,
      _value.imgPath,
      _value.releaseDate,
      _value.id,
    ]
  )
  return updated.rowsAffected !== 0
}

export const updateTagsFromItem = async (_item, _tags) => {
  let update_count = 0
  await executeQuery(`DELETE FROM tagsToItems WHERE item = ?`, [_item.id])
  for (let _tag of _tags) {
    const updated = await executeQuery(
      `INSERT INTO tagsToItems(tag, item)
    VALUES(
      ?,
      ?
    )`,
      [_tag.id, _item.id]
    )
    update_count += updated.rowsAffected
  }
  return update_count
}

const getItemWithTags = async function (arrayItem) {
  let result = []
  for (const [index, item] of arrayItem.entries()) {
    let tags = []
    const tagselect = await executeQuery(
      `SELECT Tags.id as tags_id, Tags.name, Tags.tooltip, Tags.colorcode FROM Tags JOIN tagsToItems ON Tags.id = tagsToItems.tag WHERE tagsToItems.item = ?`,
      [item.id]
    )
    tagselect.rows._array.map((tag) => {
      let newTag = new Tag(tag.tags_id, tag.name, tag.tooltip, tag.colorcode)
      tags.push(newTag)
    })

    result.push(
      new Item(
        item.id,
        item.title,
        item.description,
        (item.category = new Category(
          item.category_id,
          item.category_name,
          item.category_icon
        )),
        item.rating,
        item.review,
        item.imgPath,
        item.releaseDate,
        tags,
        item.createdAt
      )
    )
  }
  return result
}

export const getItemsWithTag = async function (_tag) {
  const itemSelect = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id JOIN tagsToItems ON Items.id = tagsToItems.item WHERE tagsToItems.tag = ? ORDER BY Category.name `,
    [_tag.id]
  )
  return getItemWithTags(itemSelect.rows._array)
}

export const getMostRecentItems = async function () {
  const itemSelect = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id WHERE Items.createdAt <= strftime('%s','now') ORDER BY Items.createdAt DESC LIMIT 5`
  )
  return getItemWithTags(itemSelect.rows._array)
}

export const searchItems = async function (searchquery) {
  const search = searchquery + '%'
  const itemSelect = await executeQuery(
    `SELECT Items.id, Items.title, Items.description, Items.rating, Items.review, Items.imgPath, Items.releaseDate, Items.createdAt, Category.id as category_id, Category.name as category_name, Category.icon as category_icon FROM Items JOIN Category ON Items.category = Category.id WHERE Items.title LIKE ? ORDER BY Items.createdAt DESC`,
    [search]
  )
  return getItemWithTags(itemSelect.rows._array)
}
