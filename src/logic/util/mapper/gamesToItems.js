import { SearchItems, SearchItemsGenres } from './searchItems';
import moment from 'moment';

/**
 * Converts the Search Results to SearchGame Objects to reduce uneccesary data
 * @param $results
 * @returns {[]}
 */
export const mapGamesToItems = ($results) => {
  let items = [];
  $results.forEach(($object) => {
    let $release_date = undefined;
    if ($object.first_release_date) {
      //$release_date = moment($object.first_release_date).format('DD.MM.YYYY');
      $release_date = $object.first_release_date
      console.log($release_date);
    }
    let $cover = $object.cover ? 'https:' + $object.cover.url : undefined;
    let mappedItem = new SearchItems(
      $object.id,
      $object.name,
      $object.summary,
      $cover,
      $release_date
    );
    items.push(mappedItem);
  });
  return items;
};
