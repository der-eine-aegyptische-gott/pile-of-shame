import { SearchItems, SearchItemsGenres } from './searchItems';
import moment from 'moment';

/**
 * Converts the Search Results to SearchMusic Objects to reduce uneccesary data
 * @param $results
 * @returns {[]}
 */
export const mapMusicToItems = ($results) => {
  let items = [];
  $results.albums.items.forEach(($object) => {
    let release_date = new Date($object.release_date);
    let name = $object.name;
    let summery = `${$object.artists[0].name} - ${name}`;
    let id = $object.id;
    let image = $object.images[0].url;
    let mappedItem = new SearchItems(
      id,
      name,
      summery,
      image,
      release_date
    );
    items.push(mappedItem);
  })
  return items;
};
