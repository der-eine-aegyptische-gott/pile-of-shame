/**
 * Creates a Wrapper Object for Search Results
 * @param _id id
 * @param _title string
 * @param _description string
 * @param _imgUrl string
 * @param _releaseDate string
 * @constructor
 */
export const SearchItems = function (
  _id = null,
  _title = '',
  _description = '',
  _imgUrl = '',
  _releaseDate = ''
) {
  this.id = _id;
  this.title = _title;
  this.description = _description;
  this.imgPath = _imgUrl;
  this.releaseDate = _releaseDate;
};

/**
 * Creates a Wrapper Object for Search Results
 * @param _name
 * @constructor
 */
export const SearchItemsGenres = function (_name) {
  this.name = _name;
};
