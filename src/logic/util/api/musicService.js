import { mapGamesToItems } from '../mapper/gamesToItems';
import {mapMusicToItems} from "../mapper/musicToItems";

/**
 * Util Function to call the Backend API to search for a Game
 * @param $game
 */
export const searchMusic = ($music) => {
  const fetchData = fetch(
    `https://pileofshame.whitefallen.de/music/search/${$music}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }
  )
    .then((response) => response.json())
    .then((json) => {
      return mapMusicToItems(json);
    });
  return fetchData;
};
