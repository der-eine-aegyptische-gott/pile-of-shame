import { mapGamesToItems } from '../mapper/gamesToItems';

/**
 * Util Function to call the Backend API to search for a Game
 * @param $game
 */
export const searchGame = ($game) => {
  const fetchData = fetch(
    `https://pileofshame.whitefallen.de/games/search/${$game}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }
  )
    .then((response) => response.json())
    .then((json) => {
      return mapGamesToItems(json);
    });
  return fetchData;
};
