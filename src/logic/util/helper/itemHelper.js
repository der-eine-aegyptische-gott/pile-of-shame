import {selectItems} from "../../ItemRepository";
import * as _ from 'lodash';
import {selectTags} from "../../TagRepository";

/**
 * Transforms the SelectItems result to be Grouped by their Categoiry, makes it easier processable for ListView
 * @returns {*}
 */
export const itemsGroupedByCategory = () => {
  return selectItems().then((data) => {
    let getCategories;
    // Get Unique Categories from Items[]
    getCategories =_.uniqBy(_.map(data, function (item) {
      return item.category
    }),function (filteredCategory) {
      return filteredCategory.id;
    });
    // Build the new Stucture
    let transformedArr = [];
    // ForEach Category make a object wich holds all Items for the Category
    getCategories.forEach((c) => {
      // Build the new Category Object
      let category = {id: c.id, name: c.name, icon: c.icon, items:[]}
      // Get the Items from the Category
      let catData = _.filter(data, function (item) {
        return item.category.id === c.id
      })
      // Add Items to their Category
      category.items = catData;
      // Add the Category to the new Sturcture
      transformedArr.push(category);
    });
    return transformedArr;
  })
}

export const getImageBySizeType = ($url, $imageSize="thumb") => {
  let $imageUrl = $url
  let $replacePart = '/t_thumb/';
  return $imageUrl.replace($replacePart, `/t_${$imageSize}/`)
}

export const groupItemsByTags = () => {
  return selectTags().then((tags) => {
    let groupedArr = [];
    groupedArr = tags
    groupedArr.forEach((groupedTag) => {
      groupedTag["items"] = [];
    })
    return addItemsToTag(groupedArr);
  })
}

export const addItemsToTag = (tagArr) => {
  return selectItems().then((items) => {
    items.forEach((item) => {
      item.tags.forEach((itemTag) => {
        const found = tagArr.findIndex((tag) => tag.id === itemTag.id);
        tagArr[found]["items"].push(item);
      })
    })
    return tagArr;
  })
}
