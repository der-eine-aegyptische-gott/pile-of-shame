create table Settings
(
    id       INTEGER
        primary key autoincrement
        unique,
    darkmode INTEGER,
    created_at  TEXT
);

INSERT INTO Settings (id, darkmode) VALUES (1, 0);

create table Category
(
    id   INTEGER
        primary key autoincrement
        unique,
    name TEXT,
    icon TEXT,
    created_at  TEXT
);

INSERT INTO Category (id, name, icon) VALUES (1, 'Videogames', 'placeholder');
INSERT INTO Category (id, name, icon) VALUES (2, 'Books', 'placeholder');
INSERT INTO Category (id, name, icon) VALUES (3, 'Movies', 'placeholder');

create table Tags
(
    id        INTEGER
        primary key autoincrement
        unique,
    name      TEXT,
    tooltip   TEXT,
    colorcode TEXT,
    created_at  TEXT
);

INSERT INTO Tags (id, name, tooltip, colorcode) VALUES (1, 'Pending', 'Needs to be done', '#30004a');
INSERT INTO Tags (id, name, tooltip, colorcode) VALUES (2, 'Done', 'Its done', '#ddaadd');
INSERT INTO Tags (id, name, tooltip, colorcode) VALUES (3, '100%', 'Get 100% in the game', '#52af52');
INSERT INTO Tags (id, name, tooltip, colorcode) VALUES (4, '100%', 'Get 100% in the game', '#52af52');

create table Items
(
    id          INTEGER
        primary key autoincrement
        unique,
    title       TEXT,
    description TEXT,
    category    INTEGER
        references Category,
    rating      REAL,
    review      TEXT,
    imgPath     TEXT,
    releaseDate TEXT,
    genres      TEXT,
    created_at  TEXT
);

INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (1, 'Hyrule Warriors Age of Calamity', 'Game from Koei Tecmo that plays 100 years before The Legend of Zelda Breath of the Wild', 1, 8, 'Nothing more or less than what you would expect from an Warriors game', 'placeholder', '1605830400', 'RPG');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (2, 'Kingdom Hearts Melody of Memory', 'Rythmic game from the Kingdom Hearts series', 1, 9, 'If you like rythmic games and kingdom hearts, dont wait', 'placeholder', '1605225600', 'Rythmic Game');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (3, 'Final Fantasy VII Remake', 'Remake of the original Final Fantasy VII that came out on PS1', 1, 9.5, 'Awe inspiring videogame with tons of action and incredible cinematics', 'placeholder', '1586476800', 'Action');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (4, 'Attack on Titan Volume 31', 'Manga about titans eating humans', 2, 10, 'Gruesome manga with a lot of conflicts about war and terror', 'placeholder', '1598313600', 'Manga');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (5, 'Die Metaphysik der Sitten', 'Book from Immanuel Kant about moral and ethic', 2, 7, 'Tough to understand', 'placeholder', '-5459270400', 'Philosophie');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (6, 'Your Name', 'A boys and a girl switch bodies every so often', 3, 9, 'Beautiful animations and story, have tissues close by', 'placeholder', '1515628800', 'Romantic');
INSERT INTO Items (id, title, description, category, rating, review, imgPath, releaseDate, genres) VALUES (7, 'Final Fantasy VII Advent Children', 'Movie that takes place after the occurences of Final Fantasy VII', 3, 9, 'Incredible nice choreography and screenplay. Must watch for Final Fantasy VII fans', 'placeholder', '1126656000', 'Action');

create table tagsToItems
(
    id   INTEGER
        primary key autoincrement
        unique,
    tag  INTEGER
        references Tags,
    item INTEGER
        references Items
);

INSERT INTO tagsToItems (id, tag, item) VALUES (1, 1, 1);
INSERT INTO tagsToItems (id, tag, item) VALUES (2, 3, 1);
INSERT INTO tagsToItems (id, tag, item) VALUES (3, 2, 2);
INSERT INTO tagsToItems (id, tag, item) VALUES (4, 3, 3);
