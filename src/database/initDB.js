import { executeQuery } from './accessor'
import * as FileSystem from 'expo-file-system'
import { getImageBySizeType } from '../logic/util/helper/itemHelper'

const initDB = async function () {
  try {
    await executeQuery(
      'CREATE TABLE IF NOT EXISTS Category (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, name TEXT, icon TEXT)'
    )

    await executeQuery(
      'CREATE TABLE IF NOT EXISTS Tags (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, name TEXT, tooltip TEXT, colorcode TEXT)'
    )

    await executeQuery(
      'CREATE TABLE IF NOT EXISTS Settings (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, theme TEXT, mostrecent INTEGER, tag INTEGER, FOREIGN KEY (tag) REFERENCES Tags(id))'
    )

    await executeQuery(
      'CREATE TABLE IF NOT EXISTS tagsToItems (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, tag INTEGER, item INTEGER, FOREIGN KEY (tag) REFERENCES Tags(id), FOREIGN KEY (item) REFERENCES Items(id))'
    )

    await executeQuery(
      'CREATE TABLE IF NOT EXISTS Items (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, category INTEGER, rating REAL, review TEXT, imgPath TEXT, releaseDate INTEGER, createdAt INTEGER, FOREIGN KEY (category) REFERENCES Category(id))'
    )

    await executeQuery(
      `INSERT INTO category(name, icon) VALUES('Videogames', 'controller-classic')`
    )

    await executeQuery(
      `INSERT INTO category(name, icon) VALUES('Books', 'book')`
    )

    await executeQuery(
      `INSERT INTO category(name, icon) VALUES('Movies', 'movie')`
    )

    await executeQuery(
      `INSERT INTO category(name, icon) VALUES('Custom', 'alpha-c-box')`
    )

    await executeQuery(
        `INSERT INTO category(name, icon) VALUES('Music', 'music-note')`
    )

    await executeQuery(
        `INSERT INTO settings(theme,mostrecent,tag)
    VALUES(
      'default',
      1,
      null
    )`
    )

  } catch (e) {
    console.error(e)
  }
}

export default initDB
