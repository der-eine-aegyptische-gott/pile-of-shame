import { executeQuery } from './accessor';
import initDB from './initDB';

const deleteDB = async function () {
  await executeQuery('DROP TABLE IF EXISTS Items');
  await executeQuery('DROP TABLE IF EXISTS Settings');
  await executeQuery('DROP TABLE IF EXISTS tagsToItems');
  await executeQuery('DROP TABLE IF EXISTS Tags');
  await executeQuery('DROP TABLE IF EXISTS Category');
};

export default deleteDB;
