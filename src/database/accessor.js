import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('pileofshame.db');

/**
 * Execute sql queries
 *
 * @param sql
 * @param params
 *
 * @returns {Promise} results
 */
export const executeQuery = (sql, params = []) => {
  return new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        sql,
        params,
        (trans, results) => {
          //console.log('Success', sql);
          //console.log(results);
          resolve(results);
        },
        (trans, error) => {
          console.log('Failed: ', sql);
          console.log(error);
          resolve(false);
        }
      );
    });
  });
};
