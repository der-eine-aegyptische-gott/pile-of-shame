# Pile-Of-Shame

## What is Pile-Of-Shame

Pile-of-Shame ist eine mittels React-Native entwickelte Mobile-App für IOS & Android zum Organisieren von Hobby Elementen.
Gestützt durch Listen, Tags und Kategorien, erlaubt Pile-of-Shame dem nutzer seine Hobbys zu organisieren und den Überblick zu behalten.

## Getting Started

1. git clone
2. npm install
3. npm run dev (Startet die App ohne Cache)
4. connect your device or emulator

### Add new Dependecies

Werden neue Abhängigkeiten hinzugefügt, oder neue Bibliotheken, sollten diese erstmal überprüft werden, ob diese kompatibel mit Expo sind.
Anschließend sollten diese dann auch nur per `expo install <package-name>` installiert werden, das sorgt dafür das für die Verwendete Expo version
die entsprechende version vom Package installiert wird.

### Colors

Primary: #34495e
Secondary: #495C6E

### Backend
Das Backend für das API-Gateway ist hier zufinden: https://gitlab.com/der-eine-aegyptische-gott/pile-of-shame-backend

### Authors

Jonas Baur, Daniel Michalak, Thomas Kammann

<div>Icon originally made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>. Design changes for our purpose made by Daniel Michalak.</div>
